﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;

namespace SourceGenerators
{
    [Generator]
    class ManifestResourceGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
            /*
#if DEBUG
            if (!Debugger.IsAttached)
            {
                Debugger.Launch();
            }
#endif 
            Debug.WriteLine("Initalize code generator");*/
        }

        public void Execute(GeneratorExecutionContext context)
        {
            StringBuilder sb = new StringBuilder(@"
using IL2CPU.API.Attribs;
using System.Collections.Generic;

namespace Curse
{
    public static class Resources
    {
");
            const string fsRootName = "VFSRoot";
            string vfsRoot = null;
            string projectName = "Curse";
            foreach (AdditionalText col in context.AdditionalFiles)
            {
                vfsRoot ??= FindParent(col.Path, fsRootName);
                //projectName ??= Path.GetFileName(Path.GetDirectoryName(vfsRoot));
                sb.Append($@"        [ManifestResourceStream(ResourceName = ""{projectName}.{fsRootName}.{Relativize(col.Path, vfsRoot).Replace(Path.DirectorySeparatorChar, '.')}"")]
        public static byte[] {GetFieldName(col.Path, vfsRoot)};
");
            }
            sb.Append(@"
        public static readonly Dictionary<string, byte[]> Files = new Dictionary<string, byte[]>()
        {
");
            foreach (AdditionalText col in context.AdditionalFiles)
                sb.Append($@"            {{""{Relativize(col.Path, vfsRoot).Replace('\\', '/')}"", {GetFieldName(col.Path, vfsRoot)}}},
");
            sb.Append(@"        };
    }
}");
            context.AddSource("Resources", sb.ToString());
        }

        private string FindParent(string path, string name)
        {
            path = Path.GetFullPath(path);
            while (Path.GetFileName(path) != name)
                path = Path.GetDirectoryName(path);
            return path + "/";
        }

        private string Relativize(string path, string reference) =>
            Uri.UnescapeDataString(new Uri(reference).MakeRelativeUri(new Uri(path)).ToString())
                .Replace('/', Path.DirectorySeparatorChar);

        private string RemoveExtension(string path) => string.Join(".", path.Split('.').Reverse().Skip(1).Reverse());

        private string GetFieldName(string colPath, string vfsRoot) => RemoveExtension(Relativize(colPath, vfsRoot)
            .Replace(Path.DirectorySeparatorChar, '_')
            .Replace(' ', '_')
            .Replace('-', '_'));
    }
}
