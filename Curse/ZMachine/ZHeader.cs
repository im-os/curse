﻿using Curse.ZMachine.Machine;

namespace Curse.ZMachine
{
    public class ZHeader
    {
        public readonly byte Version;
        public readonly ushort StartPc;
        public readonly ushort DictionaryOffset;
        public readonly ushort ObjectsOffset;
        public readonly ushort GlobalsOffset;
        public readonly ushort DynamicSize;
        public readonly ushort AbbreviationsOffset;

        public ZHeader(ZMemory aMemory)
        {
            aMemory.GetByte(0, out Version);
            aMemory.GetWord(6, out StartPc);
            aMemory.GetWord(8, out DictionaryOffset);
            aMemory.GetWord(10, out ObjectsOffset);
            aMemory.GetWord(12, out GlobalsOffset);
            aMemory.GetWord(14, out DynamicSize);
            aMemory.GetWord(24, out AbbreviationsOffset);
        }
    }
}
