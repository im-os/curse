using System;

namespace Curse.ZMachine.Machine
{
    public interface IZInput
    {
        void Read(ushort aBuffer, ushort aParse, ushort aTime, ushort aRoutine, Action callback);
    }
}