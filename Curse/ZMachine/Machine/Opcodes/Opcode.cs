﻿using System;
using System.Collections.Generic;

namespace Curse.ZMachine.Machine.Opcodes
{
    public abstract class Opcode
    {
        protected string Name { get; set; }

        protected ZMachineMain Machine { get; }

        protected Opcode(ZMachineMain machine)
        {
            Machine = machine;
        }

        public override string ToString() => Name;

        public virtual void Execute()
        {
            throw new NotImplementedException();
        }

        public virtual void Execute(ushort aArg0)
        {
            throw new NotImplementedException();
        }

        public virtual void Execute(ushort aArg0, ushort aArg1)
        {
            throw new NotImplementedException();
        }

        protected virtual void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount)
        {
            throw new NotImplementedException();
        }

        public virtual void Execute(ushort aArg0, ushort aArg1, ushort aArg2, ushort aArg3, ushort aArg4, ushort aArg5,
            ushort aArg6, ushort aArg7, ushort aArgCount, Action callback)
        {
            Execute(aArg0, aArg1, aArg2, aArg3, aArg4, aArg5, aArg6, aArg7, aArgCount);
            callback();
        }

        /// <summary>
        /// Call a routine.
        /// Save the PC and FP.
        /// Load new PC and initialize a new stack frame.
        ///
        /// Note:
        /// The caller may provide more or less arguments than the routine has.
        /// </summary>
        /// <param name="aRoutine"></param>
        /// <param name="aArgs"></param>
        /// <param name="aCallType"></param>
        protected void Call(ushort aRoutine, List<ushort> aArgs, int aCallType)
        {
            long pc = Machine.Memory.Pc;
            int i;

            int argc = aArgs.Count;
            
            Machine.Memory.Stack.AddFrame(aArgs.Count, aCallType);

            if (Machine.Header.Version <= 3)
            {
                pc = (long)aRoutine << 1;
            }
            else if (Machine.Header.Version <= 5)
            {
                pc = (long)aRoutine << 2;
            }

            Machine.Memory.Pc = pc;

            Machine.Memory.CodeByte(out byte count);

            ushort value = 0;

            for (i = 0; i < count; i++)
            {
                // V1 to V4 games provide default values for all locals.
                if (Machine.Header.Version <= 4)
                {
                    Machine.Memory.CodeWord(out value);
                }

                Machine.Memory.Stack.Push((argc-- > 0) ? aArgs[i] : value);
            }
        }

        /// <summary>
        /// Return from the current routine and restore the previous stack frame.
        /// The result may be stored (o), thrown away (1), or pushed on the stack (2).
        /// In the latter case a direct call has been finished and we must exit the interpreter loop.
        /// </summary>
        /// <param name="value"></param>
        protected void Return(ushort value)
        {
            int callType = Machine.Memory.Stack.RemoveFrame();

            if (callType == 0)
            {
                Store(value);
            }
        }

        protected void Branch(bool flag)
        {
            ushort offset;
            byte off1;

            Machine.Memory.CodeByte(out var specifier);

            off1 = (byte)(specifier & 0x3f);

            if (!flag)
            {
                specifier ^= 0x80;
            }

            if ((specifier & 0x40) == 0)
            {
                if ((off1 & 0x20) > 0)
                {
                    off1 |= 0xc0;
                }

                Machine.Memory.CodeByte(out var off2);
                offset = (ushort)((off1 << 8) | off2);
            }
            else
            {
                offset = off1;
            }

            if ((specifier & 0x80) > 0)
            {
                if (offset > 1)
                {
                    long pc = Machine.Memory.Pc;
                    pc += (short)offset - 2;
                    Machine.Memory.Pc = pc;
                }
                else
                {
                    Return(offset);
                }
            }
        }

        /// <summary>
        /// Store an operand, either as a variable or pushed on the stack.
        /// </summary>
        /// <param name="value"></param>
        protected void Store(ushort value)
        {
            Machine.Memory.CodeByte(out var variable);

            if (variable == 0)
            {
                Machine.Memory.Stack.Push(value);
            }
            else if (variable < 16)
            {
                Machine.Memory.Stack[Machine.Memory.Stack.Bp - variable] = value;
            }
            else
            {
                ushort addr = (ushort)(Machine.Header.GlobalsOffset + 2 * (variable - 16));
                Machine.Memory.SetWord(addr, value);
            }
        }
    }
}

