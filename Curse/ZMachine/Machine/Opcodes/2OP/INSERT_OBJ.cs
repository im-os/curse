﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    /// <summary>
    /// Make an object the first child of another object.
    /// </summary>
    public class InsertObj : Opcode
    {
        public InsertObj(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x0E insert_obj object destination";
        }

        public override void Execute(ushort aSourceObject, ushort aDestinationObject)
        {
            ushort obj1Addr;
            ushort obj2Addr;

            if (aSourceObject == 0)
            {
                return;
            }

            if (aDestinationObject == 0)
            {
                return;
            }

            obj1Addr = Machine.GetObjectAddress(aSourceObject);
            obj2Addr = Machine.GetObjectAddress(aDestinationObject);

            Machine.UnlinkObject(aSourceObject);

            if (Machine.Header.Version <= 3)
            {
                obj1Addr += ZObject.O1Parent;
                Machine.Memory.SetByte(obj1Addr, (byte)aDestinationObject);
                obj2Addr += ZObject.O1Child;
                Machine.Memory.GetByte(obj2Addr, out var child);
                Machine.Memory.SetByte(obj2Addr, (byte)aSourceObject);
                obj1Addr += ZObject.O1Sibling - ZObject.O1Parent;
                Machine.Memory.SetByte(obj1Addr, child);

            }
            else
            {
                obj1Addr += ZObject.O4Parent;
                Machine.Memory.SetWord(obj1Addr, aDestinationObject);
                obj2Addr += ZObject.O4Child;
                Machine.Memory.GetWord(obj2Addr, out var child);
                Machine.Memory.SetWord(obj2Addr, aSourceObject);
                obj1Addr += ZObject.O4Sibling - ZObject.O4Parent;
                Machine.Memory.SetWord(obj1Addr, child);

            }
        }
    }
}
