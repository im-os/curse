﻿namespace Curse.ZMachine.Machine.Opcodes._2OP
{
    public class Div : Opcode
    {
        public Div(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "2OP:0x17 div a b -> (result)";
        }

        public override void Execute(ushort aValue1, ushort aValue2)
        {
            Store((ushort) ((short) aValue1 / (short) aValue2));
        }
    }
}
