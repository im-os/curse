﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class PrintPaddr : Opcode
    {
        public PrintPaddr(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0D print_paddr packed-address-of-string";
        }

        public override void Execute(ushort aPackedAddress)
        {
            long byteAddress = 0;
            if (Machine.Header.Version <= 3)
            {
                byteAddress = aPackedAddress << 1;
            }
            else if (Machine.Header.Version <= 5)
            {
                byteAddress = aPackedAddress << 2;
            }

            string s = ZText.DecodeStringWithLen(byteAddress, out _);
            Machine.Output.PrintString(s);
        }
    }
}
