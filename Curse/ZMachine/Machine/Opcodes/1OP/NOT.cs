﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class Not : Opcode
    {
        public Not(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0F not value -> (result)";
        }

        public override void Execute(ushort aValue)
        {
            throw new NotImplementedException();
        }
    }
}
