﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Decrement a variable.
    /// </summary>
    public class Dec : Opcode
    {
        public Dec(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP: 0x06 dec(variable)";
        }

        public override void Execute(ushort aVariable)
        {
            if (aVariable == 0)
            {
                (Machine.Memory.Stack[Machine.Memory.Stack.Sp])--;
            }
            else if (aVariable < 16)
            {
                (Machine.Memory.Stack[Machine.Memory.Stack.Bp - aVariable])--;
            }
            else
            {
                ushort addr = (ushort) (Machine.Header.GlobalsOffset + 2 * (aVariable - 16));
                Machine.Memory.GetWord(addr, out var value);
                value--;
                Machine.Memory.SetWord(addr, value);
            }
        }
    }
}
