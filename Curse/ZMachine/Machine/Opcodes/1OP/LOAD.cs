﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Load and store the value of a variable.
    /// </summary>
    public class Load : Opcode
    {
        public Load(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0E load (variable) -> (result)";
        }

        public override void Execute(ushort aVariable)
        {
            throw new NotImplementedException();
        }
    }
}
