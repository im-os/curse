﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Jump unconditionally to the given address.
    /// </summary>
    public class Jump : Opcode
    {
        public Jump(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x0C jump ?(label)";
        }

        public override void Execute(ushort aAddress)
        {
            Machine.Memory.Pc += (short)aAddress - 2;
        }
    }
}
