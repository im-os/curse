﻿namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    /// <summary>
    /// Jump if value is zero
    /// </summary>
    public class Jz : Opcode
    {
        public Jz(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP:0x01 jz a ? (label)";
        }

        public override void Execute(ushort aValue)
        {
            Branch((short) aValue == 0);
        }
    }
}
