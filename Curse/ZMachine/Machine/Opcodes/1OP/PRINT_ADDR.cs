﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._1OP
{
    public class PrintAddr : Opcode
    {
        public PrintAddr(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "1OP: 0x07 print_addr byte-address-of-string";
        }

        public override void Execute(ushort aAdress)
        {
            throw new NotImplementedException();
        }
    }
}
