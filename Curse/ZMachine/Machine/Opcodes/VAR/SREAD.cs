﻿using System;

namespace Curse.ZMachine.Machine.Opcodes.VAR
{

    public class Sread : Opcode
    {
        public Sread(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "VAR:0x04 sread text parse, sread text parse time routine";
        }

        public override void Execute(ushort aCharBufferAddress, ushort aTokenBufferAddress, ushort aTimeoutSeconds, ushort aTimeoutCallback, ushort aArg4, ushort aArg5, ushort aArg6, ushort aArg7, ushort aArgCount, Action callback)
        {
            Machine.Input.Read(aCharBufferAddress, aTokenBufferAddress, aTimeoutSeconds, aTimeoutCallback, callback);
        }
    }
}
