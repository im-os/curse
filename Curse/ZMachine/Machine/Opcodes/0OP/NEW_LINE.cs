﻿namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    /// <summary>
    /// Print a new line.
    /// </summary>
    public class NewLine : Opcode
    {
        public NewLine(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x0B new_line";
        }

        public override void Execute()
        {
            Machine.Output.PrintString("\n");
        }
    }
}
