﻿namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class PrintRet : Opcode
    {
        public PrintRet(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x03 print_ret (literal-string)";
        }

        public override void Execute()
        {
            string s = ZText.DecodeStringWithLen((ushort)Machine.Memory.Pc, out int length);
            Machine.Memory.Pc += length;
            Machine.Output.PrintString(s);
            Machine.Output.PrintString("\n");
            Return(1);
        }
    }
}
