﻿namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    /// <summary>
    /// Return from a routine with false (0).
    /// </summary>
    public class Rfalse : Opcode
    {
        public Rfalse(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x01 rfalse";
        }

        public override void Execute()
        {
            Return(0);
        }
    }
}
