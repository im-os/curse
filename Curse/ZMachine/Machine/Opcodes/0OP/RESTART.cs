﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Restart : Opcode
    {
        public Restart(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x07 restart";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
