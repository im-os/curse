﻿using System;

namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    public class Save : Opcode
    {
        public Save(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x05 save? (label), save -> (result)";
        }

        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}
