﻿namespace Curse.ZMachine.Machine.Opcodes._0OP
{
    /// <summary>
    /// Print a string embedded in the instruction stream.
    /// </summary>
    public class Print : Opcode
    {
        public Print(ZMachineMain aMachine)
            : base(aMachine)
        {
            Name = "0OP:0x02 print (literal-string)";
        }

        public override void Execute()
        {
            string s = ZText.DecodeStringWithLen((ushort)Machine.Memory.Pc, out int length);
            Machine.Memory.Pc += length;
            Machine.Output.PrintString(s);
        }
    }
}
