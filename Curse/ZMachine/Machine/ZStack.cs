﻿using System;

namespace Curse.ZMachine.Machine
{
    public class ZStack
    {
        private readonly ushort[] _data;

        private readonly ZMemory _memory;

        public ushort Sp { get; private set; }

        public ushort Bp { get; private set; }

        public ushort Frames { get; private set; }

        public ZStack(ZMemory aMemory)
        {
            _memory = aMemory;
            _data = new ushort[32768];
            Sp = (ushort)(_data.Length);
            Bp = Sp;
            Frames = 0;
        }

        public ushort this[int index]
        {
            get
            {
                return _data[index];
            }
            set
            {
                _data[index] = value;
            }
        }

        public void AddFrame(int aArgCount, int aCallType)
        {
            long pc = _memory.Pc;

            Push((ushort)(pc >> 9));
            Push((ushort)(pc & 0x1ff));
            Push((ushort)(Bp - 1));
            Push((ushort)(aArgCount | (aCallType << 8)));

            Bp = Sp;
            Frames++;
        }

        public int RemoveFrame()
        {
            Sp = Bp;
            Frames--;

            int callType = Pop();
            callType = callType >> 8;

            Bp = Pop();
            Bp++;

            long lowPc = Pop();
            long highPc = Pop();
            highPc = highPc << 9;
            long pc = highPc | lowPc;
            _memory.Pc = pc;
            if (_memory.Pc < _memory.StartPc)
            {
                throw new Exception("Program counter is less than the start location!");
            }
            return callType;
        }

        public void Push(ushort aValue)
        {
            Sp--;
            this[Sp] = aValue;
        }

        public ushort Pop()
        {
            ushort xValue = this[Sp];
            Sp++;
            return xValue;
        }
    }
}
