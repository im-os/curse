namespace Curse.ZMachine.Machine
{
    public interface IZOutput
    {
        void PrintZscii(short aChar);
        void PrintString(string aString);
    }
}