using Cosmos.Common.Extensions;

namespace Curse.ZMachine.Machine
{
    // TODO: Create an object class instead of these extensions?
    public static class ZObject
    {
        public const ushort MaxObject = 2000;
        
        public const ushort O1Parent = 4;
        public const ushort O1Sibling = 5;
        public const ushort O1Child = 6;
        public const ushort O1PropertyOffset = 7;
        public const ushort O1Size = 9;
        
        public const ushort O4Parent = 6;
        public const ushort O4Sibling = 8;
        public const ushort O4Child = 10;
        public const ushort O4PropertyOffset = 12;
        public const ushort O4Size = 14;

        public static string GetObjectName(this ZMachineMain aMachine, ushort aObject)
        {
            ushort objAddress = aMachine.GetObjectAddress(aObject);

            if (aMachine.Header.Version <= 3)
            {
                objAddress += O1PropertyOffset;
            }
            else
            {
                objAddress += O4PropertyOffset;
            }

            aMachine.Memory.GetWord(objAddress, out ushort nameAddress);

            return ZText.DecodeString((ushort)(nameAddress + 1));
        }

        public static ushort GetObjectAddress(this ZMachineMain aMachine, ushort aObject)
        {
            if (aObject > ((aMachine.Header.Version <= (byte) 3) ? 255 : MaxObject))
            {
                aMachine.Output.PrintString("@Attempt to address illegal object ");
                aMachine.Output.PrintString(aObject.ToHex());
                aMachine.Output.PrintString(".  This is normally fatal.");
                aMachine.Output.PrintZscii(13);
            }

            if (aMachine.Header.Version <= 3)
            {
                return (ushort) (aMachine.Header.ObjectsOffset + ((aObject - 1) * O1Size + 62));
            }

            return (ushort) (aMachine.Header.ObjectsOffset + ((aObject - 1) * O4Size + 126));
        }

        public static ushort GetFirstProperty(this ZMachineMain aMachine, ushort aObject)
        {
            ushort xObjectAddress = aMachine.GetObjectAddress(aObject);
            aMachine.Memory.GetWord(xObjectAddress + 7, out ushort propTable);
            aMachine.Memory.GetByte(propTable, out byte nameSize);
            propTable += (ushort)(2 * nameSize + 1);
            return propTable;
        }

        public static ushort GetNextProperty(this ZMachineMain aMachine, ushort aProperty)
        {
            aMachine.Memory.GetByte(aProperty, out byte value);
            aProperty++;

            if (aMachine.Header.Version <= 3)
            {
                value >>= 5;
            }
            else if (!((value & 0x80) > 0))
            {
                value >>= 6;
            }
            else
            {
                aMachine.Memory.GetByte(aProperty, out value);
                value &= 0x3f;

                if (value == 0)
                {
                    value = 64;
                }

            }

            return (ushort) (aProperty + value + 1);
        }

        public static void UnlinkObject(this ZMachineMain aMachine, ushort aObject)
        {
            if (aObject == 0)
            {
                return;
            }

            ushort xObjectAddress = aMachine.GetObjectAddress(aObject);

            if (aMachine.Header.Version <= 3)
            {
                byte zero = 0;

                xObjectAddress += O1Parent;
                aMachine.Memory.GetByte(xObjectAddress, out byte xParentObject);
                if (xParentObject == 0)
                {
                    return;
                }

                aMachine.Memory.SetByte(xObjectAddress, zero);
                xObjectAddress += O1Sibling - O1Parent;
                aMachine.Memory.GetByte(xObjectAddress, out byte xOlderSibling);
                aMachine.Memory.SetByte(xObjectAddress, zero);

                ushort xParentAddress = (ushort) (aMachine.GetObjectAddress(xParentObject) + O1Child);
                aMachine.Memory.GetByte(xParentAddress, out byte xYoungerSibling);

                if (xYoungerSibling == aObject)
                {
                    aMachine.Memory.SetByte(xParentAddress, xOlderSibling);
                }
                else
                {
                    ushort xSiblingAddress;
                    do
                    {
                        xSiblingAddress = (ushort) (aMachine.GetObjectAddress(xYoungerSibling) + O1Sibling);
                        aMachine.Memory.GetByte(xSiblingAddress, out xYoungerSibling);
                    } while (xYoungerSibling != aObject);

                    aMachine.Memory.SetByte(xSiblingAddress, xOlderSibling);
                }

            }
            else
            {
                ushort zero = 0;

                xObjectAddress += O4Parent;
                aMachine.Memory.GetWord(xObjectAddress, out ushort xParentObject);
                if (xParentObject == 0)
                {
                    return;
                }

                aMachine.Memory.SetWord(xObjectAddress, zero);
                xObjectAddress += O4Sibling - O4Parent;
                aMachine.Memory.GetWord(xObjectAddress, out ushort xOlderSibling);
                aMachine.Memory.SetWord(xObjectAddress, zero);

                ushort xParentAddress = (ushort) (aMachine.GetObjectAddress(xParentObject) + O4Child);
                aMachine.Memory.GetWord(xParentAddress, out ushort xYoungerSibling);

                if (xYoungerSibling == aObject)
                {
                    aMachine.Memory.SetWord(xParentAddress, xOlderSibling);
                }
                else
                {
                    ushort xSiblingAddress;
                    do
                    {
                        xSiblingAddress = (ushort) (aMachine.GetObjectAddress(xYoungerSibling) + O4Sibling);
                        aMachine.Memory.GetWord(xYoungerSibling, out xYoungerSibling);
                    } while (xYoungerSibling != aObject);

                    aMachine.Memory.SetWord(xSiblingAddress, xOlderSibling);
                }
            }
        }      
    }
}
