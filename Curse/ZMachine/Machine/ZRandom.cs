namespace Curse.ZMachine.Machine
{
    public static class ZRandom
    {
        public static int A = 1;

        public static int Interval;
        public static int Counter;

        public static void Seed(int value)
        {
            if (value == 0)
            {
                A = 1;
                Interval = 0;
            }
            else if (value < 1000)
            {
                Counter = 0;
                Interval = value;
            }
            else
            {
                A = value;
                Interval = 0;
            }
        }
    }
}
