﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Cosmos.System.Graphics;
using Cosmos.System.Graphics.Fonts;

namespace Curse.Abstract
{
    class BufferCanvas : Canvas
    {
        public int ScreenX;
        public int ScreenY;

        private Color[] _pixelBuffer = new Color[0];
        private Color[] _pixelBufferOld = new Color[0];

        private readonly Canvas _canvas;

        public BufferCanvas(Canvas canvas)
        {
            _canvas = canvas;
            ClearImmediate();
        }

        public override List<Mode> AvailableModes => _canvas.AvailableModes;

        public override Mode DefaultGraphicMode => _canvas.DefaultGraphicMode;

        public override Mode Mode { get => _canvas.Mode; set => _canvas.Mode = value; }

        public override void Disable()
        {
            _canvas.Disable();
        }

        public override void Display()
        {
            _canvas.Display();
        }

        public void Draw()
        {
            Pen pen = new Pen(Color.Orange);
            for (int y = 0, h = ScreenY; y < h; y++)
            {
                for (int x = 0, w = ScreenX; x < w; x++)
                {
                    int l = (y * ScreenX) + x;
                    if (_pixelBuffer[l] != _pixelBufferOld[l])
                    {
                        _pixelBufferOld[l] = _pixelBuffer[l];
                        pen.Color = _pixelBuffer[l];
                        _canvas.DrawPoint(pen, x, y);
                    }
                }
            }
        }

        public override void DrawArray(Color[] colors, int x, int y, int width, int height)
        {
            Array.Copy(colors, 0, _pixelBuffer, ScreenX * y + x, width);
        }

        public override void DrawPoint(Pen pen, int x, int y)
        {
            _pixelBuffer[ScreenX * y + x] = pen.Color;
        }

        public override void DrawPoint(Pen pen, float x, float y)
        {
            DrawPoint(pen, (int)x, (int)y);
        }

        public override Color GetPointColor(int x, int y)
        {
            return _pixelBuffer[ScreenX * y + x];
        }

        public void ClearImmediate()
        {
            ScreenX = _canvas.Mode.Columns;
            ScreenY = _canvas.Mode.Rows;
            Array.Resize(ref _pixelBuffer, (ScreenX * ScreenY) + ScreenX);
            Array.Resize(ref _pixelBufferOld, (ScreenX * ScreenY) + ScreenX);
            Color c = ColorConfs.Background.Color;
            _canvas.Clear(c);
            for (int i = 0; i < (ScreenX * ScreenY) + ScreenX; i++)
            {
                _pixelBuffer[i] = c;
                _pixelBufferOld[i] = c;
            }
        }

        public override void Clear(Color color)
        {
            for (int i = 0; i < (ScreenX * ScreenY) + ScreenX; i++)
            {
                _pixelBuffer[i] = color;
            }
        }

        public void DrawStringCentered(string text, Font font, Pen pen, int y)
        {
            DrawStringCentered(text, font, pen, y, 0, ScreenX);
        }

        public void DrawStringCentered(string text, Font font, Pen pen, int y, int xStart, int xEnd)
        {
            DrawString(text, font, pen, xStart + (xEnd - xStart) / 2 - font.Width * text.Length / 2, y);
        }
    }
}
