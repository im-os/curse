﻿using System.Collections.Generic;
using System.IO;
using Cosmos.System.FileSystem.Listing;
using Cosmos.System.FileSystem.VFS;

namespace Curse.VFS
{
    class CosmosMountFileSystem : IFileSystem
    {
        private readonly DirectoryEntry _entry;
        private readonly FsPath _rootPath;

        public IEnumerable<FsPath> GetEntries(FsPath path)
        {
            if (!path.IsDirectory)
                throw new DirectoryNotFoundException();
            if (!Exists(path))
                throw new DirectoryNotFoundException();
            List<FsPath> paths = new List<FsPath>();
            foreach (DirectoryEntry entry in VFSManager.GetDirectoryListing(GetEntry(path).mFullPath))
            {
                paths.Add(GetPath(entry));
            }

            return paths;
        }

        public bool Exists(FsPath path) => path.IsFile ? VFSManager.FileExists(GetEntry(path)) : VFSManager.DirectoryExists(GetEntry(path));
        public void CreateEmpty(FsPath path)
        {
            if (path.IsFile)
                VFSManager.CreateFile(GetEntry(path).mFullPath);
            else
                VFSManager.CreateDirectory(GetEntry(path).mFullPath);
        }

        public Stream OpenFile(FsPath path)
        {
            if (!path.IsFile)
                throw new FileNotFoundException();
            return VFSManager.GetFileStream(GetEntry(path).mFullPath);
        }

        public void Delete(FsPath path)
        {
            if (path.IsFile)
                VFSManager.DeleteFile(GetEntry(path).mFullPath);
            else
                VFSManager.DeleteDirectory(GetEntry(path).mFullPath, true);
        }

        public bool ReadOnly(FsPath path) => false;

        public CosmosMountFileSystem(DirectoryEntry entry, FsPath rootPath)
        {
            _entry = entry;
            _rootPath = rootPath;
        }

        private DirectoryEntry GetEntry(FsPath path)
        {
            if (!_rootPath.IsParentOf(path))
                throw new FileNotFoundException();
            string relative = path.Path.Substring(_rootPath.Path.Length + 1).Replace(FsPath.DirectorySeparator, VFSManager.GetDirectorySeparatorChar());
            return path.IsFile ? VFSManager.GetFile(relative) : VFSManager.GetDirectory(relative);
        }

        private FsPath GetPath(DirectoryEntry entry)
        {
            string p = entry.mFullPath;
            if (!p.StartsWith(_entry.mFullPath))
                throw new FileNotFoundException();
            p = p.Substring(_entry.mFullPath.Length + 1);
            p = p.Replace(VFSManager.GetDirectorySeparatorChar(), FsPath.DirectorySeparator);
            p = p.TrimEnd('/');
            if (entry.mEntryType == DirectoryEntryTypeEnum.Directory)
                p += '/';
            return FsPath.Parse(p);
        }
    }
}
