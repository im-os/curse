﻿using Cosmos.System;
using Curse.Abstract;

namespace Curse.ImGUI
{
    public abstract class Window
    {
        public abstract void Init();
        public abstract void Close();
        public abstract void Update(ImGuiProvider imGui);

        public virtual void DrawDecorations(ImGuiProvider imGui, int titlebarSize)
        {
            imGui.DrawRectangle(0, titlebarSize, Width, Height, ColorConfs.Component, false);
            imGui.DrawRectangleAbs(0, -titlebarSize, Width + imGui.Padding, titlebarSize, ColorConfs.Component, true);
            imGui.DrawText(imGui.Padding, imGui.Padding, ColorConfs.Text, GetTitle());
            if (IsMoving) return;
            int closeStart = Width - imGui.Font.Height + imGui.Padding;
            imGui.DrawRectangleAbs(closeStart, -titlebarSize, imGui.Font.Height, titlebarSize,
                imGui.ContainsMouse(X + closeStart, Y - titlebarSize, imGui.Font.Height, titlebarSize, false) ? Pens.OrangeRed : Pens.Red,
                true);
            imGui.DrawText(closeStart + imGui.Font.Height / 2 - imGui.Font.Width / 2, titlebarSize / 2 - imGui.Font.Height / 2, ColorConfs.Text, "X");
            if (MouseState.Left.Clicking() && imGui.ContainsMouse(X + closeStart, Y - titlebarSize, imGui.Font.Height, titlebarSize, false))
                WindowManager.Close(Id);
        }
        public abstract string GetTitle();
        public override int GetHashCode() => Id.GetHashCode();

        public int X { get; set; } = 0;
        public int Y { get; set; } = 0;
        public int Id { get; set; } = 0;
        public bool IsMoving { get; set; } = false;
        public bool Movable { get; set; } = true;
        public WindowManager WindowManager { get; internal set; }
        internal int MovementOffsetX { get; set; } = 0;
        internal int MovementOffsetY { get; set; } = 0;
        internal int Width { get; set; } = 100;
        internal int Height { get; set; } = 100;
        
    }
}
