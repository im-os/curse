﻿using System.Collections.Generic;
using Curse.ImGUI;

namespace Curse.Windows
{
    class TestWindow : Window
    {
        int _v;
        bool _v2 = true;
        private string _textTest = "This is a very long line simply here for testing the text box";
        private int _textTestCursor = 0;
        private readonly List<string> _textTest2 = new List<string>();
        private int _textTest2X = 0;
        private int _textTest2Y = 0;
        public override void Close()
        {
        }

        public override string GetTitle() => nameof(TestWindow);

        public override void Init()
        {
        }

        public override void Update(ImGuiProvider imGui)
        {
            imGui.StartRow();
            imGui.Label("Piss" + _v);
            if (imGui.Button("Test", repeat: true))
                imGui.Label("Cum");
            _v = imGui.Scrollbar(_v, 0, 100);
            imGui.EndRow();
            imGui.StartRow();
            imGui.Label("Test2");
            imGui.Label(imGui.Button("Asbestos", repeat: true) ? "Meme" : "Moskau");
            imGui.Column();
            _v2 = imGui.Checkbox(_v2, "Break");
            imGui.StartRow();
            imGui.Scrollbar(imGui.Button("Increase", repeat: true) ? 50 : 25, 15, 30);
            if (_v2)
                imGui.EndRow();
            imGui.Label("Just another test");
            imGui.Column();
            imGui.Label("Last");
            imGui.ResetColumn();
            imGui.Label("Final");
            _textTest = imGui.TextBoxSingle(_textTest, ref _textTestCursor);
            imGui.TextBoxMulti(_textTest2, ref _textTest2X, ref _textTest2Y);
        }
    }
}
