﻿using System;
using Cosmos.System.Graphics;
using Cosmos.System.Graphics.Fonts;
using Curse.Abstract;

namespace Curse
{
    static class Utils
    {
        public static Mode ShowSelectPrompt(this BufferCanvas canvas, Font font)
        {
            Mode[] data = canvas.AvailableModes.ToArray();
            string[] strings = new string[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                Mode m = data[i];
                strings[i] = m.AsString();
            }
            Pen pen = ColorConfs.Text;
            int current = 0;
            for (var i = 0; i < data.Length; i++)
                if (data[i] == canvas.DefaultGraphicMode)
                    current = i;
            while (true)
            {
                // Draw
                canvas.Clear(ColorConfs.Background.Color);
                int y = 0;
                canvas.DrawString("Please choose a canvas mode", font, pen, 0, y);
                y += font.Height;
                for (int i = 0; i < data.Length; i++)
                {
                    canvas.DrawString(current == i ? "[X] " : "[ ] ", font, pen, 0, y);
                    canvas.DrawString(strings[i], font, pen, font.Width * 4, y);
                    y += font.Height;
                }

                canvas.Draw();

                //Input
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.Enter:
                        return data[current];
                    case ConsoleKey.UpArrow:
                    case ConsoleKey.LeftArrow:
                        current--;
                        if (current < 0)
                            current = data.Length - 1;
                        break;
                    case ConsoleKey.DownArrow:
                    case ConsoleKey.RightArrow:
                        current++;
                        if (current >= data.Length)
                            current = 0;
                        break;
                }
            }
        }

        private static string ColorDepthToString(ColorDepth depth) => (depth switch
        {
            ColorDepth.ColorDepth4 => nameof(ColorDepth.ColorDepth4),
            ColorDepth.ColorDepth8 => nameof(ColorDepth.ColorDepth8),
            ColorDepth.ColorDepth16 => nameof(ColorDepth.ColorDepth16),
            ColorDepth.ColorDepth24 => nameof(ColorDepth.ColorDepth24),
            ColorDepth.ColorDepth32 => nameof(ColorDepth.ColorDepth32),
            _ => nameof(ColorDepth) + "ERR",
        }).Substring(nameof(ColorDepth).Length);

        public static string AsString(this Mode m)
        {
            return $"{m.Columns}x{m.Rows}@{ColorDepthToString(m.ColorDepth)}";
        }

        public static bool IsText(this char c) => c >= 32 && c <= 126;

        public static string PrintDebug(this string text)
        {
            Cosmos.System.Kernel.PrintDebug(text);
            return text;
        }
    }
}
